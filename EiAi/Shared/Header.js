import React from 'react';
import { View, Text, StyleSheet, Image, useWindowDimensions } from 'react-native';
// import { Entypo } from '@expo/vector-icons';

const Header = () => {
    const { width } = useWindowDimensions();

    return (
        <View style={[styles.header, { width }]}>
            <View style={styles.textContainer}>
                <Text style={styles.headerTitle}><Image source={require('../assets/logo.png')} resizeMode="contain"
                    style={{ height: 50 }}

                /></Text>
                <Text style={styles.headerSubtitle}>
                    <Image source={require('../assets/profile.jpg')} resizeMode="contain"
                        style={{ height: 50 }}
                    />    </Text>
            </View>
            {/* <Entypo name="chevron-small-right" size={24} color="rgba(255, 255, 255, 0.7)" /> */}
        </View>
    );
};

const styles = StyleSheet.create({
    header: {
        backgroundColor: '#000',
        padding: 16,
        flexDirection: 'row',
        alignItems: 'left',
        justifyContent: 'space-between',
    },
    textContainer: {
        flex: 1,
    },
    headerTitle: {
        fontSize: 24,
        fontWeight: 'bold',
        color: '#fff',
    },
    headerSubtitle: {
        fontSize: 14,
        color: 'rgba(255, 255, 255, 0.7)',
        lineHeight: 20,
        marginTop: 4,
    },
});

export default Header;