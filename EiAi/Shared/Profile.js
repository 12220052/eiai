import { StyleSheet, Text, View, Image } from 'react-native'
import React from 'react'

const Profile = () => {
    return (
        <View style={styles.image}>
            <Image source={require('../assets/profile.jpg')}
                resizeMode="contain"
                style={{ height: 50 }} />
        </View>
    )
}

export default Profile

const styles = StyleSheet.create({
    image: {
        borderColor: 'url(#paint2_linear_547_4204)',

    }
})