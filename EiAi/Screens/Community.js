import React, { useState, useEffect } from 'react';
import { View, Text, Image, TouchableOpacity, Modal, TextInput, ScrollView } from 'react-native';

const users = [
    {
        id: 1,
        username: "Raiden",
        profilePictureUrl:
            "https://i.pinimg.com/474x/fc/25/50/fc2550752e7545bb709aaedecdc94b86.jpg",
    },
    {
        id: 2,
        username: "Jams",
        profilePictureUrl:
            "https://cdn.myanimelist.net/r/200x268/images/characters/7/397149.jpg?s=721210f40126090dfa0d50136df34aa7",
    },
    {
        id: 3,
        username: "KN Thinley",
        profilePictureUrl:
            "https://i.pinimg.com/474x/0b/fb/96/0bfb96a0abb1c095e2fbab04df659197.jpg",
    },
];

const data = [
    {
        id: 1,
        imageUrl: "https://embed.pixiv.net/artwork.php?illust_id=108505491",
        userId: 1,
        likes: 15,
        comments: [1, 2, 3], // IDs of comments for this item
        prompt: "An illustrious picture of a woman realized in a dancing dream",
    },
    {
        id: 2,
        imageUrl:
            "https://i.pcmag.com/imagery/articles/03a3gbCKfH8dDJnjhHLuHDf-1..v1665523315.png",
        userId: 2,
        likes: 10,
        comments: [4, 5, 6], // IDs of comments for this item
        prompt: "An illustrious picture of a woman realized in a dancing dream",
    },
    {
        id: 3,
        imageUrl:
            "https://images.nightcafe.studio//assets/tdraw-girl.jpg?tr=w-1200,c-at_max",
        userId: 2,
        likes: 12,
        comments: [7, 8, 9], // IDs of comments for this item
        prompt: "An illustrious picture of a woman realized in a dancing dream",
    },
    {
        id: 4,
        imageUrl:
            "https://mymodernmet.com/wp/wp-content/uploads/2023/01/jenni-pasanen-digital-paintings-and-ai-art-1.jpg",
        userId: 3,
        likes: 8,
        comments: [10, 11, 12], // IDs of comments for this item
        prompt: "An illustrious picture of a woman realized in a dancing dream",
    },
    {
        id: 5,
        imageUrl:
            "https://cdn.alza.cz/Foto/ImgGalery/Image/atomic-heart-keyart_1.jpg",
        userId: 3,
        likes: 8,
        comments: [13, 14, 15], // IDs of comments for this item
        prompt: "A girl watching the dress in the mirror",
    },
    {
        id: 6,
        imageUrl:
            "https://images.prismic.io/exhibition-magazine/43d4d53e-d14d-498d-83a7-cbe41dfbf207_MEDUSE_02-sRGB.jpg?auto=compress,format&rect=0,0,2209,2924&w=1800&h=2383",
        userId: 2,
        likes: 6,
        comments: [16, 17, 18],
        prompt:
            "Create a highly realistic picture with these bubbles and a monumental crystalline bottle of perfume Chanel n°5",
    },
    {
        id: 7,
        imageUrl:
            "https://images.prismic.io/exhibition-magazine/b548dc10-ae3d-4215-b810-00e9baeb8972_BROKEN_BLACK_01-sRGB.jpg?auto=compress,format&rect=0,0,2008,2658&w=1800&h=2383",
        userId: 1,
        likes: 20,
        comments: [19, 20, 21],
        prompt:
            "Broken mirror inside a Chanel number five perfume bottle, realistic photograph, frontal shot, white background, abstract",
    },
    {
        id: 8,
        imageUrl:
            "https://images.prismic.io/exhibition-magazine/00bf0aae-7298-43b5-a45d-3f40ea9fcd2b_BLACK_FLOWER_2_02-sRGB.jpg?auto=compress,format&rect=0,0,2008,2658&w=1800&h=2383",
        userId: 3,
        likes: 200,
        comments: [22, 23, 24],
        prompt:
            "Black flowers, Chanel number five perfume bottle, broken glass, backlight, black flowers, black background, deconstructed, fractal, realistic, abstract photograph, explosion, frontal shot abstract explosion, black flowers",
    },
    {
        id: 9,
        imageUrl:
            "https://i.pinimg.com/564x/7c/d1/81/7cd18147ee7f00e575137358bd3189c3.jpg",
        userId: 2,
        likes: 18,
        comments: [25, 26, 27],
        prompt:
            "Majestic smoker smoking under a dim litted room with a futuristic vibe and a serence mood. The boy is a young anime looking one with curly hair",
    },
    {
        id: 10,
        imageUrl:
            "https://i.pinimg.com/564x/d7/c5/ef/d7c5ef64715873f3529433cd0bae2883.jpg",
        userId: 1,
        likes: 15,
        comments: [28, 29, 30],
        prompt: "Surreal underwater world",
    },
    {
        id: 11,
        imageUrl:
            "https://i.pinimg.com/564x/02/28/4a/02284afff58748c00df8f5f2cd4c6adc.jpg",
        userId: 2,
        likes: 11,
        comments: [31, 32, 33],
        prompt: "Enchanting forest in autumn",
    },
    {
        id: 12,
        imageUrl:
            "https://i.pinimg.com/564x/a7/b2/07/a7b207049d7cf83efc9e0c9a0e5076a9.jpg",
        userId: 2,
        likes: 18,
        comments: [34, 35, 36],
        prompt: "A spaceship in outer space",
    },
    {
        id: 13,
        imageUrl:
            "https://i.pinimg.com/750x/10/e9/a5/10e9a557fa994359e89c9a558e6fcc64.jpg",
        userId: 3,
        likes: 9,
        comments: [37, 38, 39],
        prompt: "Mysterious ancient ruins",
    },
    {
        id: 14,
        imageUrl:
            "https://i.pinimg.com/564x/43/0e/74/430e743dbd9d37d0851de8e55d0badae.jpg",
        userId: 1,
        likes: 7,
        comments: [40, 41, 42],
        prompt: "A medieval castle on a hill",
    },
    {
        id: 15,
        imageUrl:
            "https://i.pinimg.com/564x/49/fa/db/49fadb7e7f248e90e667d6d891f154c7.jpg",
        userId: 2,
        likes: 3,
        comments: [43, 44, 45],
        prompt: "A futuristic cityscape",
    },
];

const comments = [
    {
        id: 1,
        userId: 1,
        text: "Beautiful artwork!",
        created_at: new Date(2023, 2, 15), // Unique date for the comment
        likes: 15,
    },
    {
        id: 2,
        userId: 1,
        text: "I love the colors in this.",
        created_at: new Date(2023, 2, 16), // Unique date for the comment
        likes: 10,
    },
    {
        id: 3,
        userId: 2,
        text: "Great job!",
        created_at: new Date(2023, 2, 17), // Unique date for the comment
        likes: 12,
    },
    {
        id: 4,
        userId: 2,
        text: "This is stunning!",
        created_at: new Date(2023, 2, 18), // Unique date for the comment
        likes: 8,
    },
    {
        id: 5,
        userId: 3,
        text: "I'm amazed by the details.",
        created_at: new Date(2023, 2, 19), // Unique date for the comment
        likes: 6,
    },
    {
        id: 6,
        userId: 3,
        text: "Fantastic work!",
        created_at: new Date(2023, 2, 20), // Unique date for the comment
        likes: 5,
    },
    {
        id: 7,
        userId: 1,
        text: "Wow, I love it!",
        created_at: new Date(2023, 2, 21), // Unique date for the comment
        likes: 11,
    },
    {
        id: 8,
        userId: 2,
        text: "The composition is excellent.",
        created_at: new Date(2023, 2, 22), // Unique date for the comment
        likes: 9,
    },
    {
        id: 9,
        userId: 2,
        text: "Incredible artistry!",
        created_at: new Date(2023, 2, 23), // Unique date for the comment
        likes: 7,
    },
    {
        id: 10,
        userId: 3,
        text: "This is mesmerizing!",
        created_at: new Date(2023, 2, 24), // Unique date for the comment
        likes: 13,
    },
    {
        id: 11,
        userId: 1,
        text: "I'm in awe of your talent.",
        created_at: new Date(2023, 2, 25), // Unique date for the comment
        likes: 4,
    },
    {
        id: 12,
        userId: 3,
        text: "Absolutely stunning!",
        created_at: new Date(2023, 2, 26), // Unique date for the comment
        likes: 3,
    },
    {
        id: 13,
        userId: 1,
        text: "I love the concept!",
        created_at: new Date(2023, 2, 27), // Unique date for the comment
        likes: 8,
    },
    {
        id: 14,
        userId: 2,
        text: "Great job capturing the moment.",
        created_at: new Date(2023, 2, 28), // Unique date for the comment
        likes: 7,
    },
    {
        id: 15,
        userId: 3,
        text: "Brilliant work!",
        created_at: new Date(2023, 2, 29), // Unique date for the comment
        likes: 6,
    },
];

const Community = () => {
    const [likedItems, setLikedItems] = useState([]);
    const [dataState, setDataState] = useState(data);
    const [selectedItem, setSelectedItem] = useState(null);
    const [showCommentsModal, setShowCommentsModal] = useState(false);
    const [likedComments, setLikedComments] = useState([]);
    // const [isDataSorted, setIsDataSorted] = useState(false);
    const [commentInput, setCommentInput] = useState("");
    const [modalComments, setModalComments] = useState([]);
    const [allComments, setComments] = useState(comments);

    // useEffect(() => {
    //     if (!isDataSorted) {
    //         // Sort data by likes on component mount
    //         const sortedData = [...dataState].sort((a, b) => b.likes - a.likes);
    //         setDataState(sortedData);
    //         setIsDataSorted(true); // Set isDataSorted to true after sorting
    //     }
    // }, [isDataSorted, dataState]);
    const handleLike = (itemId) => {
        const newData = [...dataState];
        const itemIndex = newData.findIndex((item) => item.id === itemId);

        if (itemIndex !== -1) {
            if (likedItems.includes(itemId)) {
                newData[itemIndex].likes -= 1;
            } else {
                newData[itemIndex].likes += 1;
            }

            setDataState(newData);
        }

        if (likedItems.includes(itemId)) {
            setLikedItems(likedItems.filter((item) => item !== itemId));
        } else {
            setLikedItems([...likedItems, itemId]);
        }
    };

    const handleLikeComment = (commentId) => {
        const newComments = [...allComments];
        const commentIndex = newComments.findIndex(
            (comment) => comment.id === commentId
        );

        if (commentIndex !== -1) {
            if (likedComments.includes(commentId)) {
                newComments[commentIndex].likes -= 1;
            } else {
                newComments[commentIndex].likes += 1;
            }

            setLikedComments(
                likedComments.includes(commentId)
                    ? likedComments.filter((id) => id !== commentId)
                    : [...likedComments, commentId]
            );
            setComments(newComments);
        }
    };

    const addComment = () => {
        if (commentInput.trim() === "") {
            return;
        }

        const newCommentId = comments.length + 1;
        const newComment = {
            id: newCommentId,
            userId: 2,
            text: commentInput,
            created_at: new Date(),
            likes: 0,
        };

        setModalComments([...modalComments, newComment]);

        const newData = [...dataState];
        const selectedItemIndex = newData.findIndex(
            (item) => item.id === selectedItem
        );

        if (selectedItemIndex !== -1) {
            newData[selectedItemIndex].comments.push(newCommentId);
        }

        setComments([...allComments, newComment]);
        setCommentInput("");
    };

    const openCommentsModal = (itemId) => {
        setSelectedItem(itemId);
        setShowCommentsModal(true);
    };

    const closeCommentsModal = () => {
        setSelectedItem(null);
        setShowCommentsModal(false);
    };

    return (
        <View >
            <ScrollView>
                {data.map((item) => (
                    <TouchableOpacity
                        key={item.id}
                        onPress={() => openCommentsModal(item.id)}
                        style={{ margin: 10 }}
                    >
                        <Image
                            source={{ uri: item.imageUrl }}
                            style={{ width: 20, height: 20 }}
                        />
                        <Text>{item.prompt}</Text>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                            <Text>{item.likes} Likes</Text>
                            <Text>{item.comments.length} Comments</Text>
                        </View>
                    </TouchableOpacity>
                ))}
            </ScrollView>

            <Modal
                visible={showCommentsModal}
                animationType="slide"
                onRequestClose={closeCommentsModal}
            >
                <View style={{ flex: 1 }}>
                    <ScrollView>
                        {/* Render comments here */}
                        {selectedItem !== null &&
                            dataState.map((item) =>
                                item.id === selectedItem ? (
                                    <View key={item.id}>
                                        {Array.isArray(item.comments) &&
                                            item.comments.map((commentId) => {
                                                const comment = comments.find((c) => c.id === commentId);
                                                const commenter = users.find(
                                                    (user) => user.id === comment.userId
                                                );

                                                const currentTime = new Date();
                                                const commentTime = comment.created_at;
                                                const timeDifference = currentTime - commentTime;
                                                const hoursAgo = Math.floor(
                                                    timeDifference / (60 * 60 * 1000)
                                                );

                                                return (
                                                    <View
                                                        key={commentId}
                                                        style={{ borderBottomWidth: 1, borderBottomColor: '#ccc', paddingVertical: 10 }}
                                                    >
                                                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                            <Image
                                                                source={{ uri: commenter.profilePictureUrl }}
                                                                style={{ width: 30, height: 30, borderRadius: 15 }}
                                                            />
                                                            <Text style={{ marginLeft: 10 }}>{commenter.username}</Text>
                                                        </View>
                                                        <Text>{comment.text}</Text>
                                                        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 5 }}>
                                                            <Text>{hoursAgo === 0 ? "Few moments ago" : `${hoursAgo} ${hoursAgo === 1 ? "hour" : "hours"} ago`}</Text>
                                                            <TouchableOpacity onPress={() => handleLikeComment(commentId)}>
                                                                {/* Render like icon here */}
                                                            </TouchableOpacity>
                                                            <Text>{comment.likes}</Text>
                                                        </View>
                                                    </View>
                                                );
                                            })}
                                    </View>
                                ) : null
                            )}
                    </ScrollView>

                    <View style={{ padding: 10, borderTopWidth: 1, borderTopColor: '#ccc' }}>
                        <TextInput
                            placeholder="Add a comment..."
                            value={commentInput}
                            onChangeText={setCommentInput}
                            style={{ borderWidth: 1, borderColor: '#ccc', borderRadius: 5, padding: 5 }}
                        />
                        <TouchableOpacity onPress={addComment} style={{ backgroundColor: 'blue', padding: 10, borderRadius: 5, marginTop: 5 }}>
                            <Text style={{ color: 'white' }}>Add Comment</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
        </View>
    );
};

export default Community;
