import { StyleSheet, Text, View, Image } from 'react-native';
import React from 'react';
import Heading1 from '../Shared/Heading1';

const LandingPage = () => {
    return (
        <View style={styles.container}>
            <Heading1 />
            <Image stylw={styles.Logo}
                source={require('../assets/logo.png')}
                resizeMode='contain'
            />

        </View>
    );
};

export default LandingPage;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#000000',
        alignItems: 'center',
        justifyContent: 'center',
    },
    Logo: {
        width: '90%',

    }

});
