import { StyleSheet, Text, View, Header } from 'react-native'
import React from 'react'
import Heading1 from '../Shared/Heading1'
// import { Header } from 'react-native/Libraries/NewAppScreen'

const CommunityPage = () => {
    return (
        <View style={styles.contain}>
            {/* <Header> */}
            <Heading1 />
            {/* </Header> */}
        </View>
    )
}

export default CommunityPage

const styles = StyleSheet.create({
    contain: {
        backgroundColor: "#000000"
    }
})