// import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
// import LandingPage from './Screens/LandingPage';
// import Community from './Screens/Community';
import CommunityPage from './Screens/CommunityPage';
import Header from './Shared/Header';

export default function App() {
  return (
    <View style={styles.container}>
      {/* <LandingPage />
       */}
      <Header />

    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: '#ffffff',
    // alignItems: 'center',
    // justifyContent: 'center',
  },
});
